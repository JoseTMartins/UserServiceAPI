package accountservice.repositories;

import accountservice.entities.User;
import accountservice.starter.Application;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@DataJpaTest
public class UserRepositoryTest {

	@Autowired
	private TestEntityManager entityManager;
	@Autowired
	private UserRepository userRepository;

	private final User user1 = new User();

	@Before
	public void setup() {
		// create a new user
		user1.setFirstName("John");
		user1.setLastName("Doe");
		user1.setEmail("johndoe@gmail.com");
	}

	@Test
	public void whenFindById_thenReturnUser() {
		// if persisted
		User persistedUser = entityManager.persist(user1);
		// we should get an user
		Optional<User> userFound = userRepository.findById(persistedUser.getId());
		// and then
			User user = userFound.get();
			assertThat(user.getFirstName(), is(equalTo("John")));
			assertThat(user.getLastName(), is(equalTo("Doe")));
			assertThat(user.getEmail(), is(equalTo("johndoe@gmail.com")));
		
	}
}
