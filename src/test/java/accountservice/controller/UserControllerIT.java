package accountservice.controller;

import accountservice.dto.ErrorResponse;
import accountservice.dto.UserResponse;
import accountservice.entities.Roles;
import accountservice.entities.User;
import accountservice.services.UserEmailService;
import accountservice.services.UserRegistrationEventListener;
import accountservice.services.UserService;
import accountservice.starter.Application;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@AutoConfigureMockMvc
public class UserControllerIT {

    @Autowired
    private UserRegistrationEventListener userRegistrationEventListener;
    @Autowired
    private UserEmailService emailService;
    @Autowired
    private UserService userservice;
    @Autowired
    private ObjectMapper mapper;
    @Autowired
    private MockMvc mockMvc;


    @Test
    public void newValidUser_returnsCreatedUser_withGuestStatus_And_returns_201_HttpCreatedStatus() throws Exception {
        final String userJSON = "{\"firstName\": \"John\", \"lastName\": \"Doe\", \"email\": \"johnDoe@gmail.com\"}";

        final MvcResult result = mockMvc.perform(
                MockMvcRequestBuilders.post("/api/user")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(userJSON))
                        .andExpect(status().isCreated()).andReturn();

        final UserResponse response = mapper.readValue(result.getResponse().getContentAsByteArray(), UserResponse.class);
        assertThat(response.getFirstName()).contains("John");
        assertThat(response.getLastName()).contains("Doe");
        assertThat(response.getEmail()).contains("johnDoe@gmail.com");
        assertThat(response.getRole().contains(Roles.GUEST.getRole()));
        assertThat(response.getId()).isNotNull();
    }


    @Test
    public void newInvalidUser_withEmptyFirstName_fails_And_returns_409_HttpConflictStatus() throws Exception {
        final String userJSON = "{\"firstName\": \"\", \"lastName\": \"Doe\", \"email\": \"johnDoe@gmail.com\"}";

        final MvcResult result = mockMvc.perform(
                MockMvcRequestBuilders.post("/api/user")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(userJSON))
                .andExpect(status().isConflict()).andReturn();

        final ErrorResponse response = mapper.readValue(result.getResponse().getContentAsByteArray(), ErrorResponse.class);
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getMessage().contains("Validation failed"));
    }

    @Test
    public void newInvalidUser_withEmptyLastName_fails_And_returns_409_HttpConflictStatus() throws Exception {
        final String userJSON = "{\"firstName\": \"John\", \"lastName\": \"\", \"email\": \"johnDoe@gmail.com\"}";

        final MvcResult result = mockMvc.perform(
                MockMvcRequestBuilders.post("/api/user")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(userJSON))
                .andExpect(status().isConflict()).andReturn();

        final ErrorResponse response = mapper.readValue(result.getResponse().getContentAsByteArray(), ErrorResponse.class);
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getMessage().contains("Validation failed"));
    }

    @Test
    public void newInvalidUser_withEmptyEmail_fails_And_returns_409_HttpConflictStatus() throws Exception {
        final String userJSON = "{\"firstName\": \"John\", \"lastName\": \"Doe\", \"email\": \"\"}";

        final MvcResult result = mockMvc.perform(
                MockMvcRequestBuilders.post("/api/user")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(userJSON))
                .andExpect(status().isConflict()).andReturn();

        final ErrorResponse response = mapper.readValue(result.getResponse().getContentAsByteArray(), ErrorResponse.class);
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getMessage().contains("Validation failed"));
    }

    @Test
    public void newValidUser_WithAnAlreadyUsedEmail_Fails_And_returns_409_HttpConflictStatus() throws Exception {
        final String userOneJSON = "{\"firstName\": \"Michael\", \"lastName\": \"Jackson\", \"email\": \"michaeljackson@gmail.com\"}";
        final String userTwoJSON = "{\"firstName\": \"Peter\", \"lastName\": \"Gabriel\", \"email\": \"michaeljackson@gmail.com\"}";


        final MvcResult resultOne = mockMvc.perform(
                MockMvcRequestBuilders.post("/api/user")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(userOneJSON))
                .andExpect(status().isCreated()).andReturn();

        final MvcResult resultTwo = mockMvc.perform(
                MockMvcRequestBuilders.post("/api/user")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(userTwoJSON))
                .andExpect(status().isConflict()).andReturn();

        //first user shall pass
        final UserResponse userResponse = mapper.readValue(resultOne.getResponse().getContentAsByteArray(), UserResponse.class);
        assertThat(userResponse.getFirstName()).contains("Michael");
        assertThat(userResponse.getLastName()).contains("Jackson");
        assertThat(userResponse.getEmail()).contains("michaeljackson@gmail.com");
        assertThat(userResponse.getId()).isNotNull();

        //Second user shall fail because has the same email as the previously inserted.
        final ErrorResponse response = mapper.readValue(resultTwo.getResponse().getContentAsByteArray(), ErrorResponse.class);
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getMessage().contains("user persistence save error"));
    }

    @Test
    public void RegistrationsEventIsFiredWhenNewUserCreated(){

        User user=new User();
        user.setFirstName("dummy");
        user.setLastName("dummy");
        user.setEmail("dummy@dummy");
        userservice.persist(user);
        Assert.assertTrue(userRegistrationEventListener.getEventFired());
    }

    @Test
    public void emailEventIsFiredWhenNewUserCreated() throws ExecutionException,InterruptedException {

        User user=new User();
        user.setFirstName("dummy2");
        user.setLastName("dummy2");
        user.setEmail("dummy2@dummy");
        userservice.persist(user);
        CompletableFuture<Boolean> emailSent=emailService.isEmailSent();
        Assert.assertTrue(emailSent.get());
    }

}
