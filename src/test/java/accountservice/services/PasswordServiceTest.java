package accountservice.services;

import org.junit.Assert;
import org.junit.Test;

public class PasswordServiceTest {

    private final PasswordService passwordService=new PasswordService();

    @Test
    public void generatedPasswordIsNotEmpty_and_isEightCharLong(){
        String generatedPassword=passwordService.getNewPassword();

        Assert.assertNotNull(generatedPassword);
        Assert.assertEquals(8,generatedPassword.length());
    }

    @Test
    public void generateHashFromPasswordIsNotEmpty_and_isSixtyCharsLong(){
        String generatedPassword=passwordService.getNewPassword();
        String hashedPassword=passwordService.hashPassword(generatedPassword);

        Assert.assertNotNull(hashedPassword);
        Assert.assertEquals(60,hashedPassword.length());
    }

    @Test
    public void plainTextPasswordValidatesAgainstTheRealHashedPassword(){
        String generatedPassword=passwordService.getNewPassword();
        String hashedPassword=passwordService.hashPassword(generatedPassword);

        Assert.assertTrue(passwordService.checkPassword(generatedPassword,hashedPassword));
    }

    @Test
    public void plainTextPasswordValidatesAgainstFalseHashedPassword(){
        String generatedPasswordOne=passwordService.getNewPassword();
        String generatedPasswordTwo=passwordService.getNewPassword();
        String hashedPassword=passwordService.hashPassword(generatedPasswordTwo);

        Assert.assertFalse(passwordService.checkPassword(generatedPasswordOne,hashedPassword));
    }
}
