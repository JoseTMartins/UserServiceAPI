package accountservice.services;


import lombok.AllArgsConstructor;
import lombok.Data;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class PasswordService {


    public PasswordDetails generatePasswordDetails(){
        String plainTextPassword= getNewPassword();
        String hashedPassword=hashPassword(plainTextPassword);
        return new PasswordDetails(plainTextPassword,hashedPassword);
    }
    /*
    * Generates an 8 char random String
    * with chars between #(dec:35) and z(dec 122)
    * This is the user password
    * */
    public String getNewPassword(){
        int leftLimit = 47; // char '/'
        int rightLimit = 122; // char 'z'
        int targetStringLength = 8;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for (int i = 0; i < targetStringLength; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        return buffer.toString();
    }

    /*
     * Hashes a plaintext password using Bcrypt library
     * this is the one that should be stored in the database
     * */
    public  String hashPassword(String password_plaintext) {

        int workload = 12;  // Define the BCrypt workload to use when generating password hashes. 10-31 is a valid value.
        String salt = BCrypt.gensalt(workload);
        return BCrypt.hashpw(password_plaintext, salt);
    }

    /*
    * Compares a plaintext password with the equivalent
    * hashed counterpart to ensure they match
    */
    public boolean checkPassword(String password_plaintext, String stored_hash) {

        if(null == stored_hash || !stored_hash.startsWith("$2a$"))
            throw new java.lang.IllegalArgumentException("Invalid hash provided for comparison");

        return BCrypt.checkpw(password_plaintext, stored_hash);
    }

}

@Data
@AllArgsConstructor
class PasswordDetails{
    private String plainTextPassword;
    private String HashedPassword;
}