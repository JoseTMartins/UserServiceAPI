package accountservice.services;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Slf4j
@AllArgsConstructor
@Component
public class UserRegistrationEventListener {

    private final UserEmailService emailService;
    private boolean eventFired;

    @Autowired
    public UserRegistrationEventListener(UserEmailService emailService){
        this.emailService=emailService;
    }

    @EventListener
    public void handleRegistrationTasks(UserRegistrationEvent event) throws InterruptedException{
        eventFired=true;
        log.debug("New User registration event triggered");
        try {
            emailService.send(event.getUser(), event.getPlaintextPassword());
        }catch (InterruptedException ex){
            throw new InterruptedException("New User Registration email sending failed");
        }
    }

    public boolean getEventFired(){
        return eventFired;
    }

}
