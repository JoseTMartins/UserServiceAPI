package accountservice.services;

import accountservice.entities.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;

@Slf4j
@Service
public class UserEmailService {

    private final CompletableFuture<Boolean> emailSent=new CompletableFuture<>();

    @Async("threadPoolTaskExecutor")
    void send(final User user,final String userPlainTextPassword) throws InterruptedException {
        emailSent.complete(true);
        log.info("Sending email to user :"+ user.getEmail() + " WITH PASSWORD :" +userPlainTextPassword);
      //TODO implement email send logic depending on server.
    }

    public CompletableFuture<Boolean> isEmailSent() {
            return emailSent;
    }
}
