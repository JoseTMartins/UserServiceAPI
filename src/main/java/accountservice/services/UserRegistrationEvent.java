package accountservice.services;

import accountservice.entities.User;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
class UserRegistrationEvent {

    private User user;
    private String plaintextPassword;
}
