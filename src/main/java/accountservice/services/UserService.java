package accountservice.services;

import accountservice.converters.UserConverter;
import accountservice.dto.UserResponse;
import accountservice.entities.Roles;
import accountservice.entities.User;
import accountservice.exceptions.PersistenceFailException;
import accountservice.exceptions.ResourceNotFoundException;
import accountservice.repositories.UserRepository;
import accountservice.repositories.UserRoleRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Service
public class UserService {

	private final UserRepository userRepository;
	private final UserRoleRepository userRoleRepository;
	private final UserConverter userConverter;
	private final PasswordService passwordService;
	private final ApplicationEventPublisher publisher;
	private PasswordDetails passwordDetails;

	//acts as a wrapper for user save.UserController should use this one
	public UserResponse persist(final User user) {
		setDefaultRoleFor(user);
		getDefaultPasswordDetailsFor();
		user.setPassword(passwordDetails.getHashedPassword());
		//User persistedUser=save(user);
		return userConverter.convert(save(user));
	}

	private User save(final User user) throws PersistenceFailException {
		User persistedUser;
        try {
            if (user.getRole()==null || user.getPassword()==null){
                throw new PersistenceFailException("Persistence Error-Invalid user role or plain text password");
            }
            persistedUser = userRepository.save(user);
		} catch (Exception ex) {
			log.error("Error persisting user");
			throw new PersistenceFailException("user persistence save error");
		}
		log.info("persisting user" + persistedUser);
        publishUserRegistrationEvent(user, passwordDetails.getPlainTextPassword());
		return persistedUser;
	}

	public List<UserResponse> getAll() {
		List<User> userList;
		try {
			userList = userRepository.findAll();
		} catch (Exception ex) {
			log.error("Error retrieving userList from persistence");
			throw new PersistenceFailException("user persistence read error");
		}
		log.info("returning user list");
		return userConverter.convert(userList);
	}

	public UserResponse findById(final Long userId) {
		Optional<User> foundUser = userRepository.findById(userId);
		if (foundUser.isPresent()) {
			return userConverter.convert(foundUser.get());
		} else {
			log.error("Resource with id:" + userId + " not found");
			throw new ResourceNotFoundException("resource id:" + userId + " not found");
		}
	}

	public UserResponse updateUser(final Long userId, final User userData) {
		Optional<User> foundUser = userRepository.findById(userId);
		if (foundUser.isPresent()) {
			User user = foundUser.get();
			user.setFirstName(userData.getFirstName());
			user.setLastName(userData.getLastName());
			user.setEmail(userData.getEmail());

			//UserResponse updatedUser = persist(user);
			return persist(user);
		} else {
			log.error("resource with id:" + userId + " not found");
			throw new ResourceNotFoundException("resource id:" + userId + " not found");
		}
	}

	public void deleteUser(final Long userId) {
		Optional<User> foundUser = userRepository.findById(userId);
		if (foundUser.isPresent()) {
			try {
				User userToDelete = foundUser.get();
				userRepository.delete(userToDelete);
			} catch (Exception ex) {
				log.error("Error deleting user" + userId + " from persistence");
				throw new PersistenceFailException("user persistence delete error");
			}
		} else {
			log.error("resource with id:" + userId + " not found");
			throw new ResourceNotFoundException("resource id:" + userId + " not found");
		}
	}

	private void setDefaultRoleFor(User user){
		if (user.getRole()==null) {
			user.setRole(userRoleRepository.findByRole(Roles.GUEST.getRole()));
		}
	}

	private void getDefaultPasswordDetailsFor() {
        passwordDetails= passwordService.generatePasswordDetails();
    }

	private void publishUserRegistrationEvent(final User user,final String plainTextPassword){
		publisher.publishEvent(new UserRegistrationEvent(user,plainTextPassword));
	}
}

