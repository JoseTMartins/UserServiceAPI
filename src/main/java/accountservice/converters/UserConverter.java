package accountservice.converters;

import accountservice.dto.UserResponse;
import accountservice.entities.User;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;


@Component
public class UserConverter implements Converter<User, UserResponse> {

	@Override
	public UserResponse convert(final User user) {
		
		return UserResponse.builder()
				.id(user.getId())
				.firstName(user.getFirstName())
				.lastName(user.getLastName())
				.email(user.getEmail())
				.role(user.getRole().getRole())
				.build();
	}
	
	public List<UserResponse> convert(final List<User> userList){
		return userList.stream().map(this::convert).collect(Collectors.toList());
	}
}
