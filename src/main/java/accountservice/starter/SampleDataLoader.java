package accountservice.starter;

import accountservice.entities.UserRole;
import accountservice.repositories.UserRoleRepository;
import lombok.AllArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class SampleDataLoader implements ApplicationRunner {

    private final UserRoleRepository userRoleRepository;

    public void run(ApplicationArguments args){
        userRoleRepository.save(new UserRole("guest"));
        userRoleRepository.save(new UserRole("operator"));
        userRoleRepository.save(new UserRole("super-operator"));
        userRoleRepository.save(new UserRole("admin"));
        userRoleRepository.save(new UserRole("super-admin"));
    }

}

