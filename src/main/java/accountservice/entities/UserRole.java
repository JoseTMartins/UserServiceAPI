package accountservice.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name="user_role")
@Data
@NoArgsConstructor
public class UserRole {

    public UserRole (String role){
        this.role=role;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private long id;

    @Column(name="role",nullable = false)
    @Size(min=2,max=25)
    @NotBlank
    private String role;

}
