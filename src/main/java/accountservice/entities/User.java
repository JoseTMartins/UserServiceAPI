package accountservice.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="user")
@EntityListeners(AuditingEntityListener.class)
@Data
@NoArgsConstructor
@JsonIgnoreProperties(value = {"createdAt", "updatedAt","password"}, allowGetters = true)
public class User implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;
	
	@Column(name="firstname", nullable=false)
	@Size(min = 2, max = 50)
	@NotBlank
	private String firstName;
	
	@Column(name="lastname", nullable=false)
	@Size(min = 2, max = 50)
	@NotBlank
	private String lastName;
	
	@Column(name="email", unique=true, nullable=false)
	@Size(min = 5, max = 50)
	@NotBlank
	private String email;

	@Column(name="pwd")
	private String password;

	@ManyToOne
	private UserRole role;
	
	@Column(name="created_on", nullable=false, updatable=false)
	@Temporal(TemporalType.TIMESTAMP)
	@CreatedDate
	private Date createdAt;
	
	@Column(name="updated_on",nullable=false)
	@Temporal(TemporalType.TIMESTAMP)
	@LastModifiedDate
	private Date updatedAt;
}
