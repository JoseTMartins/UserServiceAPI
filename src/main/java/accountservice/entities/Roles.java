package accountservice.entities;

public enum Roles {

    GUEST("guest"),
    OPERATOR("operator"),
    SUPER_OPERATOR("super-operator"),
    ADMIN("admin"),
    SUPER_ADMIN("super-admin");

    private final String role;

    Roles (String role){
        this.role=role;
    }
    public String getRole(){
        return role;
    }
//    public boolean equalsName(String name) {
//        return name.equals(name);
//    }


}
