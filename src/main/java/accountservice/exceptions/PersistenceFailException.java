package accountservice.exceptions;

public class PersistenceFailException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public PersistenceFailException(String message) {
		super(message);
	}

}
