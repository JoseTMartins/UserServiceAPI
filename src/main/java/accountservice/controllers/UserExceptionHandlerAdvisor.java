package accountservice.controllers;

import accountservice.dto.ErrorResponse;
import accountservice.exceptions.PersistenceFailException;
import accountservice.exceptions.ResourceNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;

@Slf4j
@ControllerAdvice(assignableTypes=UserController.class)
public class UserExceptionHandlerAdvisor {

	@ExceptionHandler(PersistenceFailException.class)
	public ResponseEntity<ErrorResponse> handlePersistenceErrorException(final PersistenceFailException ex) {
		log.error("Persistence Failure{}", ex);
		final ErrorResponse errorResponse = new ErrorResponse(
											false, LocalDateTime.now(), ex.getMessage(), HttpStatus.CONFLICT);
		
		return new ResponseEntity<>(errorResponse, HttpStatus.CONFLICT);
	}

	@ExceptionHandler(ResourceNotFoundException.class)
	public ResponseEntity<ErrorResponse> handleResourceNotFoundException(final ResourceNotFoundException ex) {
		log.error("resource not found{}", ex);
		final ErrorResponse errorResponse = new ErrorResponse(
											false, LocalDateTime.now(), ex.getMessage(), HttpStatus.NOT_FOUND);
		
		return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(NullPointerException.class)
	public ResponseEntity<ErrorResponse> handleNullPointerException(final NullPointerException ex) {
		log.error("unknown error{}", ex);
		final ErrorResponse errorResponse = new ErrorResponse(
											false, LocalDateTime.now(), ex.getMessage(), HttpStatus.CONFLICT);
		
		return new ResponseEntity<>(errorResponse, HttpStatus.CONFLICT);
	}
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<ErrorResponse> handleMethodArgumentNotValidException(final MethodArgumentNotValidException ex){
		log.error("Validation Error{}", ex);
		final ErrorResponse errorResponse = new ErrorResponse(
				false, LocalDateTime.now(), "Validation failed", HttpStatus.CONFLICT);
		return new ResponseEntity<>(errorResponse, HttpStatus.CONFLICT);
	}

}
