package accountservice.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import accountservice.dto.UserResponse;
import accountservice.entities.User;
import accountservice.services.UserService;
import lombok.RequiredArgsConstructor;


@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class UserController {

	private final UserService userservice;

	@GetMapping("/users")
	public List<UserResponse> getAllUsers() {
		return userservice.getAll();
	}

	@PostMapping("/user")
	public ResponseEntity<UserResponse> createUser(@Valid @RequestBody User user) {
		return new ResponseEntity<>(userservice.persist(user), HttpStatus.CREATED);
	}

	@GetMapping("/user/{id}")
	public ResponseEntity<UserResponse> getUser(@PathVariable("id") Long userId) {
		return new ResponseEntity<>(userservice.findById(userId),HttpStatus.OK);
	}

	@PutMapping("/user/{id}")
	public ResponseEntity<UserResponse> updateUser(@PathVariable("id") Long userId, @Valid @RequestBody User userData) {
		return new ResponseEntity<>(userservice.updateUser(userId, userData),HttpStatus.OK);
	}

	@DeleteMapping("/user/{id}")
	public ResponseEntity<?> deleteUser(@PathVariable("id") Long userId) {
		userservice.deleteUser(userId);
		return ResponseEntity.status(HttpStatus.GONE).build();
	}
}
